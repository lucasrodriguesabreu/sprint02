package br.com.lucas.streams;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class OperacoesIntermediarias {

    public static void main(String[] args) {


        List<Integer> lista = Arrays.asList(0, 1, 5, 8, 9, 1, 4, 7, 6, 6, 9, 9);
        //antigamente fariamos dessa forma pra imprimir a lista
        /*for(Integer integer : lista){
            System.out.println(integer);
        }
        //hoje em dia
        lista.stream().forEach(e -> System.out.println(e));

        System.out.println("****************************************");*/

        /**
         * STREAM
         * O stream é um fluxo de dados, onde efetuamos loops implícitos
         */
        /**
         * MÉTODO SKIP
         *
         * método skip pula ou ignora os dados, nesse caso vai ignorar os dois primeiros números da lista.
         * o método skip é uma operação intermediária, ou seja, podemos fazer várias dessas operações antes
         * de fechar a stream
         */
        /*lista.stream()
                .skip(2)
                .forEach(e -> System.out.println(e));*/
        /**
         * MÉTODO LIMIT
         *
         * é uma operação intermediária, ou seja, podemos fazer várias operações antes de fechar a stream
         * esse método limita quantos elementos deseja processar, no exemplo abaixo ele processa os primeiros 2 números
         */
        /*lista.stream()
                .limit(2)
                .forEach(e -> System.out.println(e));

        /*
        forEach é um método final, após implementá-lo no stream, o fluxo é encerrado.
         */

        /**
         * MÉTODO DISTINCT
         * também uma operação intermediária, sua função é não permitir que o stream processe elementos repetidos
         o método distinct utiliza a comparação do equals e hashcode para saber se o elemento está repetido ou não.
         */
/*
        lista.stream()
                .distinct()
                .forEach(e -> System.out.println(e));
*/
        /**
         * MÉTODO FILTER
         *
         * o método filter é um método personalizado, quando precisamos de um filtro mais específico e personalizavel, utilizamos ele
         * no exemplo abaixo vamos verificar se o número é par
         */
/*
        lista.stream()
                .filter(e -> e % 2 == 0)
                .forEach(e -> System.out.println(e));
*/

        /**
         * MÉTODO MAP
         *
         * apesar do nome, não há relação com a interface map.
         *
         * o método map faz transformação de dados. no exemplo abaixo, há um exemplo em que os números da lista serão multiplicados por dois.
         *
         * a lista original não é modificada, ela continua com os mesmos elementos que ela tinha originalmente
         * o map não faz a transformação dentro da lista, apenas no dado que está passando no stream.
         *
         * podemos comprovar isso imprimindo a lista original no final
         */
        /*
        lista.stream()
                .map(e -> e * 2)
                .forEach(e -> System.out.println(e));

        System.out.println("********************");
        lista.stream()
                .forEach(e -> System.out.println(e));
        */
        /*
        DICAS
        o ideal é sempre aplicar os filtros antes de utilizar o map que é a transformação de dados.

        lembrando que após efetuar uma operação final, não é possível armazenar o stream em uma variável e chamar novamente o foreach
         */

        Stream<Integer> map = lista.stream()
                .limit(3)
                .map(e -> e * 2);
        map.forEach(e -> System.out.println(e));
        map.forEach(e -> System.out.println(e));
    }
}
