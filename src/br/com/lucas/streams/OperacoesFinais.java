package br.com.lucas.streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
//OPERAÇÕES FINAIS
public class OperacoesFinais {

    public static void main(String[] args) {


        List<Integer> lista = Arrays.asList(2, 1, 5, 8, 9, 1, 4, 7, 6, 6, 9, 9);

        /*Stream<Integer> map = lista.stream()
                .limit(3)
                .map(e -> e * 2);
        map.forEach(e -> System.out.println(e));*/

        /**
         * Das operações finais o foreach não é a mais utilizada, pois não retorna nada
         * para imprimir no console ele faz algum sentido
         */

        /**
         * COUNT
         *
         * O count retorna a quantidade de itens que estão no stream
         */
        /*
        long count = lista.stream()
                .map(e -> e * 2)
                .count();
        System.out.println(count);*/

        //no exemplo abaixo vou contar quantos números pares possui no stream
        /*long count = lista.stream()
                .filter(e -> e % 2 == 0)
                .count();
        System.out.println(count);*/

        /**
         * MIN
         *
         * Verifica qual é o menor elemento da lista
         * No caso havia um filtro para retornar apenas números pares, tirando o filtro ele retorna o menor número
         * mesmo sendo impar
         */
        /*
        Optional<Integer> min = lista.stream()
                //.filter(e -> e % 2 == 0)
                .min(Comparator.naturalOrder());
        System.out.println(min.get());
        */
        /**
         * MAX
         *
         * Mesmo lógica do MIN
         */
        /*
        Optional<Integer> max = lista.stream()
                //.filter(e -> e % 2 == 0)
                .max(Comparator.naturalOrder());
        System.out.println(max.get());
        */

        /**
         * COLLECT
         * É uma operação final que permite maior customização
         */
        /**
         * TOLIST
         * o toList, por exemplo, pega todos os resultados da stream e salva em uma variável
         *
         * no exemplo abaixo peguei os números pares, multipliquei por 3 e salvei em uma lista
         */

        List<Integer> novaLista = lista.stream()
                .filter(e -> e % 2 == 0)
                .map(e -> e * 3)
                .collect(Collectors.toList());
        System.out.println(novaLista);

        /**
         * COLLECT - GROUPINGBY
         *
         * O groupingBy(), cria uma mapa por algum critério desejado
         * Nesse caso foi implementado um retorno para separar números ímpares e pares
         *
         * No segundo exemplo criamos um filtro para separar os números divisiveis por 3
         */
        /*
        Map<Boolean, List<Integer>> mapa = lista.stream()
                .map(e -> e * 3)
                .collect(Collectors.groupingBy(e -> !(e % 2 == 0)));
        System.out.println(mapa);
        */
        /*
        Map<Integer, List<Integer>> mapa = lista.stream()
                .collect(Collectors.groupingBy(e -> e % 3));
        System.out.println(mapa);
        */
        /**
         * COLLECT - JOINING()
         *
         * O joining() só trabalha com o agrupamento de strings.
         * Nesse caso vamos converter a lista de inteiros em string.
         * Podemos também separar as strings, por exemplo, com virgula
         */

        String collect = lista.stream()
                .map(e -> String.valueOf(e))
                .collect(Collectors.joining(", "));
        System.out.println(collect);
    }
}
