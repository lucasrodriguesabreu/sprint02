package br.com.lucas.generics_;

public class Main {

    public static void main(String[] args) {
//        ClasseLegal<Integer> obj = new ClasseLegal<>(10);
//        ClasseLegal<Double> obj2 = new ClasseLegal<>(20.0);

        /*não é possível utilizar tipo primitivo, pois não é genérico
        ClasseLegal<int> obj = new ClasseLegal<>(10);
        ClasseLegal<double> obj2 = new ClasseLegal<>(20.0);*/

        ClasseLegal<Integer, Double> obj = new ClasseLegal<>(10, 20.0);


        obj.mostraTipo();
//        obj2.mostraTipo();
    }
}
