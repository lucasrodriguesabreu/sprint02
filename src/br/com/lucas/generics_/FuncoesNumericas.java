package br.com.lucas.generics_;

public class FuncoesNumericas<T extends Number> {

    T obj;

    public FuncoesNumericas(T obj) {
        this.obj = obj;
    }

    /*vai ser apresentado erro, pois o java não tem como saber se o que vai
    * ser passado dentro do T é de fato um double
    * se for passado uma string, por exemplo, como iremos efetuar a multiplicação?
    * como vamos garantir que o que será passado é um númerico?
    * vamos estender dentro do <> para o Number*/
    double quadrado(){
        return obj.intValue() * obj.doubleValue();
    }
}
