package br.com.lucas.generics_;

public class MeusIntegers {

    Integer i;

    public MeusIntegers(Integer i) {
        this.i = i;
    }

    void imprime(){
        System.out.println(i);
    }
}
