package br.com.lucas.generics_;

//nesse caso estamos passando dois tipos de dados
public class ClasseLegal<T, V> {

    T obj;
    V obj2;

    public ClasseLegal(T obj, V obj2) {
        this.obj = obj;
        this.obj2 = obj2;
    }

    void mostraTipo(){
        System.out.println(obj.getClass().getName());
        System.out.println(obj2.getClass().getName());
    }
}
