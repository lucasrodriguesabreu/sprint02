package br.com.lucas.generics_;

import java.util.ArrayList;
import java.util.List;

public class Main_bkp {
    /**
     * A principal vantagem de utilizar Generics é que caso ocorra erro em algum trecho de código,
     * esse erro é exibido antes mesmo do código ser compilado, evitando dificuldades quando o
     * programa está em execução.
     *
     * Em resumo, o Generics existe somente em tempo de compilação
     */

    /* COM GENERICS */
  /*  List<Apple> box = ...;
    Apple apple = box.get(0);

    /* SEM GENERICS */
//    List box = new ArrayList();
    //SEM GENERICS
    /*
    public static void main(String[] args) {

        //É uma lista mãe, aceita qualquer informação
        List lista = new ArrayList<>();
        lista.add("String");
        lista.add("1L");
        lista.add("1223");
        System.out.println(lista);

        for (Object obj : lista) {
            if (obj instanceof String) {
                System.out.println(obj);
            }
            if (obj instanceof Long) {
                System.out.println(obj);
            }
        }
    }
    */
    /*
    após inserir essa regra <String>, ao chamar o add já é possível identificar que o tipo será string
     */
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        lista.add("String");
        lista.add("1L");
        lista.add("1223");
        System.out.println(lista);

        //vamos adicionar uma lista e um long

        for (String obj : lista) {
            System.out.println(lista);
        }
    }
}
