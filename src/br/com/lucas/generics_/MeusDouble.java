package br.com.lucas.generics_;

public class MeusDouble {

    Double i;

    public MeusDouble(Double i) {
        this.i = i;
    }

    void imprime(){
        System.out.println(i);
    }
}
