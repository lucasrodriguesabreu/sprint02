package br.com.lucas.wildcards;

public class Main {

    public static void main(String[] args) {

        SelecionadorAleatorio<Pessoa> sap = new SelecionadorAleatorio<Pessoa>();
        sap.addElemento(new Pessoa("Lucas", 27));
        sap.addElemento(new Pessoa("João", 27));
        sap.addElemento(new Pessoa("Marcos", 27));

        SelecionadorAleatorio<PessoaJuridica> sapj = new SelecionadorAleatorio<PessoaJuridica>();
        sapj.addElemento(new PessoaJuridica("Google", 20, "000.000.000-00"));
        sapj.addElemento(new PessoaJuridica("Facebook", 20, "000.000.000-01"));
        sapj.addElemento(new PessoaJuridica("Twitter", 20, "000.000.000-02"));

        SelecionadorAleatorio<Fundacao> saf = new SelecionadorAleatorio<Fundacao>();
        saf.addElemento(new Fundacao("Fundação de Teste", 35, "111.111.111-10", 20, "Estatuto teste"));
        saf.addElemento(new Fundacao("Fundação de Teste 1", 35, "111.111.111-11", 21, "Estatuto teste 1"));
        saf.addElemento(new Fundacao("Fundação de Teste 2", 35, "111.111.111-12", 22, "Estatuto teste 2"));

        //https://youtu.be/do-mETsgox8?t=840

    }
}
