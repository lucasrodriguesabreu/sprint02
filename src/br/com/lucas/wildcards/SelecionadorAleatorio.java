package br.com.lucas.wildcards;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SelecionadorAleatorio<T> {

    private List<T> elementos;
    private Random random;

    public SelecionadorAleatorio() {
        this.elementos = new ArrayList<T>();
        this.random = new Random();
    }

    public void addElemento(T elem){
        this.elementos.add(elem);
    }

    public void delElemento(T elem){
        this.elementos.remove(elem);
    }

    public T getElemento(int posicao){
        return this.elementos.get(posicao);
    }

    public T sortear(){
        int posicao = this.random.nextInt(this.elementos.size());
        return this.elementos.get(posicao);
    }
}
