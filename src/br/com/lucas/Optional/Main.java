package br.com.lucas.Optional;

import java.util.Optional;
import java.util.stream.Stream;

public class Main {

    /**
     * Usamos o Optional para que não seja necessário ficar retornando null ao chamar nosso método
     * No exemplo a seguir vamos converter números em string
     * No primeiro exemplo estamos fazendo antes do Java 8, quando não havia a opção de utilizar o Optional
     */
    /*
    public static void main(String[] args) {

        String s = "1";
        Integer numero = converteEmNumero(s);
        System.out.println(numero);

    }

    public static Integer converteEmNumero(String numeroStr){
        return Integer.valueOf(numeroStr);
    }
    */


    //antigamente tratariamos essa exceção com try/catch dessa forma
    /*public static Optional<Integer> converteEmNumero(String numeroStr) {
        try {
            Integer integer = Integer.valueOf(numeroStr);
            return Optional.of(integer);
        } catch (Exception e) {
            return null;
        }
    }*/

    /**
     * isPresent
     * <p>
     * é o método para verificar se a string está sendo preenchido ou não
     * <p>
     * get()
     * <p>
     * é o método para chamar o que está sendo passado
     * lembrando que o get é a forma mais simples de buscar o valor dentro do optional
     * <p>
     * ifPresent()
     * <p>
     * recebe uma expressão lambda, ele só executa a expressão lambda se tiver algum valor dentro do Optional
     * se passar algo que pode ser convertido, ele retorna o próprio valor
     * <p>
     * orElse()
     * <p>
     * é possível definir um valor default caso o Optional não possa ser convertido
     * <p>
     * orElseGet()
     * <p>
     * mesma lógica do orElse(), mas também recebe uma função lambda
     * <p>
     * orElseThrow
     * <p>
     * também carrega a mesma lógica do orElse, mas é possível através dele lançar uma exceção dentro de uma expressão lambda
     */
/*
    public static void main(String[] args) {
        //Se passar apenas uma letra funciona
        String s = "Conhecendo Optional";
        Optional<Integer> numero = converteEmNumero(s);
        System.out.println(numero.isPresent());

    }

 */
    /*
    public static void main(String[] args) {
        //Se passar apenas uma letra funciona
        String s = "1";
        Optional<Integer> numero = converteEmNumero(s);
        System.out.println(numero.get());

    }
*/
    /*
    public static void main(String[] args) {
        String s = "1";
        converteEmNumero(s).ifPresent(n -> System.out.println(n));
    }
    */
    /*
    public static void main(String[] args) {
        String s = "Conhecendo Optional";
        Integer numero = converteEmNumero(s).orElse(2);
        System.out.println(numero);
    }
    */

    /*
    public static void main(String[] args) {
        String s = "Conhecendo Optional";
        Integer numero = converteEmNumero(s)
                .orElseGet(() -> 5);
        System.out.println(numero);
    }
    */
    public static void main(String[] args) {
        /*
        String s = "Conhecendo Optional!";
        Integer numero = converteEmNumero(s)
                .orElseThrow(() -> new NullPointerException("Valor vazio!"));
        System.out.println(numero);
    }*/
        //Usando Streams
        Stream.of(1, 2, 3)
                .findFirst()
                .ifPresent(n -> System.out.println(n));
    }

    public static Optional<Integer> converteEmNumero (String numeroStr){
        try {
            Integer integer = Integer.valueOf(numeroStr);
            return Optional.of(integer);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}