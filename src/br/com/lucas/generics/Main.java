package br.com.lucas.generics;

public class Main {

    public static void main(String[] args) {


        Pessoa p1 = new Pessoa("Lucas", 27);
        Pessoa p2 = new Pessoa("Maria", 25);
        Pessoa p3 = new Pessoa("Carla", 31);
        Pessoa p4 = new Pessoa("Roger", 25);
        Pessoa p5 = new Pessoa("Matheus", 26);

        SelecionadorAleatorio<Pessoa> sa = new SelecionadorAleatorio<Pessoa>();

        sa.addElemento(p1);
        sa.addElemento(p2);
        sa.addElemento(p3);
        sa.addElemento(p4);
        sa.addElemento(p5);

        Pessoa p = sa.sortear();

        System.out.println("O sorteado foi " + p.getNome() + ", parabéns!");

        System.out.println("**************************************");

        SelecionadorAleatorio<Integer> si = new SelecionadorAleatorio<Integer>();

        si.addElemento(1);
        si.addElemento(2);
        si.addElemento(3);
        si.addElemento(4);
        si.addElemento(12145454);

        System.out.println("O número sorteado foi " + si.sortear() + ", parabéns!");

        System.out.println("**************************************");

        SelecionadorAleatorio<String> ss = new SelecionadorAleatorio<String>();

        ss.addElemento("A");
        ss.addElemento("B");
        ss.addElemento("C");
        ss.addElemento("D");

        System.out.println("A letra do alfabeto sorteada foi " + ss.sortear() + ", parabéns!");

        System.out.println("**************************************");

        SelecionadorAleatorio<Double> sd = new SelecionadorAleatorio<Double>();

        sd.addElemento(1.0);
        sd.addElemento(2.0);
        sd.addElemento(3.0);
        sd.addElemento(4.0);

        System.out.println("O double sorteado foi " + sd.sortear() + ", parabéns!");
    }
}
