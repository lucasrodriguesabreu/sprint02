package br.com.lucas.wildcard2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    /*é o caractere coring do generics*/

    public static void main(String[] args) {
        //Criando um array com dois cachorros
        Cachorro[] cachorros = {new Cachorro(), new Cachorro()};
        Gato[] gatos = {new Gato(), new Gato()};
        //consultarAnimais(cachorros);
        //consultarAnimais(gatos);

        List<Cachorro> cachorroList = new ArrayList<>();
        cachorroList.add(new Cachorro());
        List<Gato> gatoList = new ArrayList<>();
        gatoList.add(new Gato());

        consultarAnimaisList(cachorroList);
        consultarAnimaisList(gatoList);
        //ordenarLista(cachorroList);

    }

    public static void consultarAnimais(Animal[] animais){
        for(Animal animal : animais){
            animal.consulta();
        }
    }

    /*nesse caso estamos usando wildcards estendendo animal, ou seja, posso receber lista de classes filhas de Animal
    * mas ao estender eu me comprometo a não adicionar nada*/
    public static void consultarAnimaisList(List<? extends Animal> animais){
        for (Animal animal : animais){
            animal.consulta();
        }
        /*se eu tentar adicionar um cachorro?
        * vai apresentar erro, pois me comprometi através do extends a não adicionar mais nada, terei que criar outro método*/
        //animais.add(new Cachorro());
    }

    /*usando wildcards para poder adicionar cachorros ou super classes de cachorros
    por isso a vantagem de utilizar a palavra super, para que possa ficar bem genérico*/
    public static void consultarCachorrosList(List<? super Cachorro> cachorroList){
        Cachorro c1 = new Cachorro();
        Animal a1 = new Cachorro();
        Object o = new Cachorro();
        cachorroList.add(new Cachorro());
    }
}