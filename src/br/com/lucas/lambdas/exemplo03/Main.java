package br.com.lucas.lambdas.exemplo03;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        //Imprimindo essa lista antes das lambdas
/*
        List<Integer> asList = Arrays.asList(0, 1, 2, 3, 4, 5);

        for(Integer integer : asList){
            System.out.println(integer);*/

        List<Integer> asList = Arrays.asList(0, 1, 2, 3, 4, 5);
        asList.stream().forEach(e -> System.out.println(e));
    }
}
