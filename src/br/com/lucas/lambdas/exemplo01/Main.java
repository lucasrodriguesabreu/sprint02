package br.com.lucas.lambdas.exemplo01;

public class Main {

    public static void main(String[] args) {

        /**
         * Antes do Java 8 era comum visualizar códigos como esse abaixo
         */

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Olá mundo!");
            }
        }).run();

        /**
         * Agora vamos executar o mesmo código acima, porém, com lambdas
         * Após converter o código para lambda, é possível entender que o código fica menor
         * Para o Java, o código em lambda é igual ao primeiro, ou seja, vai executar a mesma coisa.
         *
         * Como Java vai saber que é um runnable e estamos implementando o método run?
         *
         * O Java entende através do conceito SAM - Single Abstract Method
         * O que é SAM? Esse conceito representa que qualquer interface que tenha apenas um método abstrato.
         * Um exemplo de SAM? A Runnable, que possui um único método abstrato que é o Run()
         *
         * Outro exemplo é a ActionListener, o método actionPerformed() não está escrito que é abstract,
         * mas em interface quando não está escrito que é abstract é porque ele é mesmo assim.
         *
         * Tendo em vista que a interface Runnable só tem um método, o que estamos implementando no código só pode ser
         * a execução do método run()
         *
         * A mesma lógica é aplicada para addActionListener() com o actionPerformed().
         *
         *
         *
         */

        new Thread(() -> System.out.println("Olá mundo!")).run();

        /**
         * mas o que significa esse () que vem antes de -> ?
         * esse é o parametro que essa função recebe.
         * nesse exemplo, o método run não recebe nenhum parametro, por isso abrimos e fechamos parametro vazio
         *
         * no caso do ActionListener recebe o 'e', por isso foi passado o e.
         *
         * O formato da expressão lambda é da seguinte forma:
         *
         * a primeira parte são os argumentos que a função recebe e a parte da direita é a implementação desse método.
         *
         * posso usar Lambda somente com FunctionalInterfaces? NÃO!
         *
         * Posso usar lambda com interfaces com qualquer interface que siga o padrão SAM, mesmo que NÃO tenha a anotação
         * @FunctionalInterface
         */

        /**
         * BENEFÍCIOS
         *
         * O Java implementou lambdas só para reduzir a quantidade de código? NÃO!
         *
         * O Java implementou lambdas para podermos usar Streams
         */

        /**
         * STREAMS
         *
         * Streams é um fluxo de dados
         *
         */



    }
}
